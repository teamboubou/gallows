defmodule GallowsWeb.HangmanView do
  use GallowsWeb, :view

  @responses %{
    :won              => { :success, "You Won!" },
    :lost             => { :danger, "You Lost!" },
    :good_guess       => { :success, "Good guess!" },
    :bad_guess        => { :warning, "Bad guess!" },
    :already_guessed  => { :info, "You already guessed that" },
  }

  def game_state(state) do
    @responses[state]
    |> alert()
  end

  defp alert(nil), do: ""
  defp alert({class, message}) do
    """
    <div class="alert alert-#{class}">
      #{message}
    </div>
    """
    |> raw()
  end

  def make_word(letters) do
    letters |> Enum.join(" ")
  end

  def game_over?(state) do
    Enum.member?([:won, :lost], state)   
  end

  def new_game_button(conn) do
    button("New Game", to: hangman_path(conn, :create_game))
  end

  def turn(left, target) when left <= target do
    "opacity: 1"
  end

  def turn(_left, _target) do
    "opacity: 0.1"
  end
end
